try:
    import json
except ImportError:
    import simplejson as json

try:
    from urllib.request import urlopen
except ImportError:
    from urllib2 import urlopen

LANGUAGES = ["en", "es", "fr", "de"]

class GW2API:

    @staticmethod
    def api_request(arg):
        url = "https://api.guildwars2.com/v1/" + arg
        return json.loads(urlopen(url).read())

    @staticmethod
    def get_all_items():
        return GW2API.api_request('items.json')

    @staticmethod
    def get_item(id, lang="en"):
        if lang not in LANGUAGES:
            lang = "en"
        return GW2API.api_request('item_details.json?item_id=%s&lang=%s' % (id, lang))

    @staticmethod
    def get_all_recipes():
        return GW2API.api_request('recipes.json')

    @staticmethod
    def get_recipe(id, lang="en"):
        if lang not in LANGUAGES:
            lang = "en"
        return GW2API.api_request('recipe_details.json?recipe_id=%s&lang=%s' % (id, lang))

    @staticmethod
    def get_all_events():
        return GW2API.api_request('events.json')

    @staticmethod
    def get_event_names(lang="en"):
        if lang not in LANGUAGES:
            lang = "en"
        return GW2API.api_request('event_names.json?lang=%s' % lang)

    @staticmethod
    def get_events(**kwargs):
        return GW2API.api_request('events.json?' + '&'.join(kwarg + "=" + str(kwargs[kwarg]) for kwarg in kwargs))

    @staticmethod
    def get_world_names(lang="en"):
        if lang not in LANGUAGES:
            lang = "en"
        return GW2API.api_request('world_names.json?lang=%s' % lang)

    @staticmethod
    def get_map_names(lang="en"):
        if lang not in LANGUAGES:
            lang = "en"
        return GW2API.api_request('map_names.json?lang=%s' % lang)
        
    @staticmethod
    def get_wvw_matches():
        return GW2API.api_request('wvw/matches.json')

    @staticmethod
    def get_match_details(id):
        return GW2API.api_request('wvw/match_details.json?match_id=%s' % id)

    @staticmethod
    def get_objective_names(lang="en"):
        if lang not in LANGUAGES:
            lang = "en"
        return GW2API.api_request('wvw/objective_names.json?lang=%s' % lang)
        